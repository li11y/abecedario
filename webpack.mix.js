let mix = require('laravel-mix');

// Third party
let BannerPlugin      = require('webpack').BannerPlugin;
let CopyWebpackPlugin = require('copy-webpack-plugin');
// let ImageminPlugin    = require('imagemin-webpack-plugin').default;
// let imageminMozjpeg   = require('imagemin-mozjpeg');

let gulp              = require('gulp');
let htmlmin           = require('gulp-htmlmin');
let jsonmin           = require('gulp-jsonminify');

// get git info from command line
let git_commit  = require('child_process').execSync('git rev-parse --short HEAD').toString().replace(/\r?\n?/g, '');

let PACKAGE = require('./package.json');
let banner = PACKAGE.name + ' - v' + PACKAGE.version + ' (' + git_commit + ') | ' +
  '(c) 2013 - ' + new Date().getFullYear() + '  ' + PACKAGE.author + ' | ' +
  PACKAGE.license.type + ' | ' + PACKAGE.homepage;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. By default, we are compiling the Sass/Less
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public');
mix.disableNotifications();

mix.browserSync({
    open: false,
    proxy: false,
    watch: true,
    server: "./public"
});

mix.webpackConfig( {
    plugins: [
        new BannerPlugin(banner)
        //Compress images
        // new CopyWebpackPlugin([{
        //     from: 'resources/assets/img',
        //     to: 'img/',
        // }]),
        // new ImageminPlugin({
        //     test: /\.(png|gif|svg)$/i,
        //     pngquant: {
        //         quality: '65-80'
        //     },
        //     test: /\.(jpe?g)$/i,
        //     pngquant: {
        //         quality: '65-80'
        //     },
        //     plugins: [
        //         imageminMozjpeg({
        //             quality: 80,
        //             //Set the maximum memory to use in kbytes
        //             // maxMemory: 1000 * 512
        //         })
        //     ]
        // })
    ],
});

mix.js('src/assets/js/custom.js', 'public/js/app.js')
   .less('src/assets/less/custom.less', 'public/css/app.css')
   .copy('src/assets/favicon', 'public/favicon')
   .copy('src/service-worker.js', 'public/service-worker.js')
   .copy('src/assets/sounds', 'public/sounds');

gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('public'));


if (mix.inProduction()) {
    //mix.version();
} else {

    if (process.env.WATCH === "true") {

        // Minify html!
        gulp.task('html', function (){
            gulp.src('src/*.html')
                .pipe(htmlmin({collapseWhitespace: true}))
                .pipe(gulp.dest('public'));
        });
        gulp.watch('src/*.html', ['html']);
    }
}
