(function() {
  const colors = ['#000', '#ff1900', '#f2ea04', '#5bf103', '#0cf2f2', '#ac09e8', '#ed0b8b'];

  // Generate alphabet from A to Z
  for ( i = 65; i <= 90; i++ ) {
     char = document.createElement('div');
     char.innerHTML = String.fromCharCode(i);

     x = Math.floor((Math.random()*5) + 1);

     char.style.color = colors[x];
     document.getElementById('abecedario').appendChild(char);

     char.addEventListener('click', playAudio);
  }

  function playAudio(e) {
    var char   = e.target.innerText.charCodeAt();
    var player = document.getElementById('player');

    player.setAttribute('src', `sounds/${char}.mp3`);
    player.play();
  }

  function changeColor() {
    var divs = document.getElementsByTagName('div'), i=divs.length;

    while (i--) {
      x = Math.floor((Math.random()*5) + 1);
      divs[i].style.color= colors[x];
    }
  }

  // Randomly change every character color
  setInterval(changeColor, 5000);
})();
